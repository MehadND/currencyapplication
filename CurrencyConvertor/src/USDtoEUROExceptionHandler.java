
public class USDtoEUROExceptionHandler extends Exception {
	
	String message;
	
	public USDtoEUROExceptionHandler(String errMessage)
	{
		message = errMessage;
	}
	
	public String getMessage() 
	{
		return message;
	}
}
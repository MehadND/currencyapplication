import junit.framework.TestCase;

public class USDtoEUROTest extends TestCase 
{
	USDtoEURO testObject = new USDtoEURO();
	
	// Test Number: 1
	// Test Objective: To multiply $100 by an exchange rate of 0.5
	// Test input(s): exchangeRate = 0.5, amountUSD = 100
	// Test expected output(s): 50
	
	public void testUSDtoEURO00001() 
	{	
		try 
		{
			assertEquals(50, testObject.convertCurrency(0.5,100));
		} 
		catch (USDtoEUROExceptionHandler e) 
		{
			fail("Expection not expected");
		}
	}
	
	// Test Number: 2
	// Test Objective: To multiply $10,000 by an exchange rate of 1.5
	// Test input(s): exchangeRate = 1.5, amountUSD = 10000
	// Test expected output(s): 15000
		
	public void testUSDtoEURO00010() 
	{	
		assertEquals(15000, testObject.convertCurrency(1.5,10000));
	}
		
	// Test Number: 3
	// Test Objective: To multiply $MinDouble by an exchange rate of MinDouble
	// Test input(s): exchangeRate = Double.MIN_VALUE, amountUSD = Double.MIN_VALUE
	// Test expected output(s): //Error message "Invalid Exchange Rate"
		
	public void testUSDtoEURO00011() 
	{	
		try 
		{
			testObject.convertCurrency(Double.MIN_VALUE,Double.MIN_VALUE);
			fail("Expection Expected");
		} 
		catch (USDtoEUROExceptionHandler e) 
		{
			assertEquals("Invalid Exchange Rate",e.getMessage());
		}
	}
	
	// Test Number: 4
	// Test Objective: To multiply $99.99 by an exchange rate of 0.49
	// Test input(s): exchangeRate = 0.49, amountUSD = 99.99
	// Test expected output(s): //Error message
		
	public void testUSDtoEURO00100() 
	{	
		assertEquals(50, testObject.convertCurrency(0.49,99.99));
	}
		
	// Test Number: 5
	// Test Objective: To multiply $10,001 by an exchange rate of 1.51
	// Test input(s): exchangeRate = 1.51, amountUSD = 10001
	// Test expected output(s): //Error message
			
	public void testUSDtoEURO00101() 
	{	
		assertEquals(15000, testObject.convertCurrency(1.51,10001));
	}
			
	// Test Number: 6
	// Test Objective: To multiply $MaxDouble by an exchange rate of MaxDouble
	// Test input(s): exchangeRate = MaxDouble, amountUSD = MaxDouble
	// Test expected output(s): //Error message
			
	public void testUSDtoEURO00110() 
	{	
		assertEquals(50, testObject.convertCurrency(Double.MAX_VALUE,Double.MAX_VALUE));
	}
}
